# Drone image workflow

We use drones for terrestrial surveying in Iceland. This directory contains the scripts we use for that. (Note that many can be used for images other than those derived from drones.)

README is a WIP.

## Dependencies

Make sure these are installed on whatever machine you're running:

* ImageMagick
* FFmpeg
* OpenDroneMap (ODM): Creates both 3D and 2D meshes of geographical data from structured 2D images. It is installed as a container through Docker on the Field Science VM.
    * maybe WebODM
* Structure from Motion (SfM): Using the open source SMVS software we produce 3D images from structured 2D imagery taken by the UAVs. These can be used to create digital elevation maps of the site. We do SfM via WebODM.
* [MajorTom-FlightPlan](https://gitlab.cluster.earlham.edu/field-science/uav_app): The code here will generally be run after MajorTom work is done - i.e., the workflow is: make+upload+fly a flight plan, transfer images, then use this software as you wish.

## Stages and variations in the drone workflow

Our drones gather a series of images, most of which have ground control points (GCP's) that are consistent and can be used to align images in the stitching process.

### Preprocessing

This might include:

* Removing all images from takeoff and landing, which are not useful for most purposes
* Entropy and blur correction or removal

### Assembly
For example, orthographic projection, 2D stitching with ODM. We create 2D stitched images of our NIR data and 3D meshes of our VLI data using SfM.

## Quantities we care about

* Visible light images: Our default.
* Near-infrared (NIR) images and artifacts: After assembly, we can run normalized difference vegetation index (NDVI) scripts; see ndvi directory in this repo. We apply a series of algorithms to the VLI & NIR orthographics to generate false-color images which are used by archaeologists to identify likely areas for further excavation.

