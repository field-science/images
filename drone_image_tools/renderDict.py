import argparse
import generateDict

# SPECIFY COMMAND LINE ARGUMENTS
parser = argparse.ArgumentParser(description="jpeg altitude histogram")
parser.add_argument('-i', '--input', help='Enter the input jpeg directory', required=True, dest="inDir")
args = parser.parse_args()

# COMMAND LINE ARGUMENT VARIABLES
inputDir = args.inDir

x = [] # values to plot
jpegAltitudeDict = generateDict.gen(inputDir) # get image dictionary
for image in jpegAltitudeDict:
    print('IMAGE PATH: ' + str(image) + '   ALT: ' + str(jpegAltitudeDict[image]))
    x.append(jpegAltitudeDict[image])
