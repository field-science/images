#!/usr/bin/env bash

# if param is null usage and bail 

# wrap in a find loop that looks for the type of the param? 
# or find loop that calls this script one file at a time? (this seems more flexible)
# what if it is run from another directory? 
# 

export SIZE=48

for FILE in *.mp4 ;
	do echo $FILE ; 
	vcs --numcaps 8 --jpeg --height 800 --title `ls $PWD/$FILE | awk -F/ '{print $(NF-2)"/"$(NF-1)"/"$(NF)}'` --user "" --override 'signature="_"' --override 'pts_title='$SIZE'' --override 'pts_tstamps='$SIZE'' --override 'pts_meta='$SIZE'' --override 'pts_sign='$SIZE'' --override 'font_all=Courier' $FILE ; 
	open $FILE.jpg ; 
done


# --title `echo $PWD | awk -F/ '{print $(NF-1)"/"$(NF)}'`
# chop dir off at both ends, ls not working as ads
# full URL of file, makes the sheet portable 