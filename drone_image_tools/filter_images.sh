# Filter Images by Modulus N
# Porter L, 2022

# === Print Help Info ===
usage()
{
echo ""
cat << EOF
usage example: ./filter_images.sh -d folder_where_images_live/ -f 2
-d    | --directory    (required)    The relative path to the directory where the images live
-f    | --filtermod    (required)    The modulus number to use (even Nth image will be copied)
-h    | --help                       Brings up usage
EOF
echo ""
}

# === Parse Input Args ===
while [[ $# -gt 0 ]]
do
arg="$1"

case $arg in
    -d|--directory)
    	IMAGES_DIRECTORY=$2
    	shift
    	shift
    ;;
    -f|--filtermod)
    	FILTER="$2"
    	shift
    	shift
    ;;
    -h|--help)
    	usage
    	exit 0
    ;;
    *)
    shift # Ignore the rest
    ;;
esac
done

# === Set Up Vars ===
TMP_IMAGES_DIRECTORY="filtered_mod_$FILTER"
COUNT=0
mkdir $IMAGES_DIRECTORY/$TMP_IMAGES_DIRECTORY

# === Loop Over Files ===
for im in $IMAGES_DIRECTORY/*
do
	if [[ $(($COUNT % $FILTER)) -eq 0 ]]; then
		echo "--> Copied $im.";
		cp $im $IMAGES_DIRECTORY/$TMP_IMAGES_DIRECTORY
	else
		echo "--> Ignored $im.";
	fi
	COUNT=$(($COUNT+1))
done