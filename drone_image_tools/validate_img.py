import PIL
from PIL import Image
import sys
import os
from tqdm import tqdm

ACCEPT_TYPES = [
    ".dmg",
    ".jpg",
    ".jpeg",
    ".png",
]

def print_ls(arr):
    outstr = ""
    for item in arr:
        outstr += item + ", "
    outstr = outstr[:-2]
    print(outstr)


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("Something went wrong...\nThis script expects a single arguement, a directory name.")
        print("\nExample:   python3 validate_img.py img_directory/\n")
    else:
        PATH = sys.argv[1]

        valid_img = []
        invalid_img = []
        non_img = []

        file_count = sum(len(files) for _, _, files in os.walk(PATH))  # Get the number
        pbar = tqdm(total=file_count)
        
        for subdir, dirs, files in os.walk(PATH):
            for file in files:
                pbar.update(1)
                filepath = subdir + "/" + file
                extension = os.path.splitext(filepath)[1]
                img_path = subdir + "/" + file
                if extension.lower() in ACCEPT_TYPES: 
                    try:
                        img = Image.open(img_path)
                        img.verify()
                        valid_img.append(img_path)
                    except Exception:
                        invalid_img.append(img_path)
                else:
                    non_img.append(img_path)

        print("")
        print("VALID IMAGES (" + str(len(valid_img)) + "):")
        print_ls(valid_img)
        print("")
        print("INVALID IMAGES (" + str(len(invalid_img)) + "):")
        print_ls(invalid_img)
        print("")
        print("NON-IMAGE FILES (" + str(len(non_img)) + "):")
        print_ls(non_img)
        print("")