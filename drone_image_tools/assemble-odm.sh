#!/usr/bin/env bash
#
# 2D or 3D, low or high resolution, ODM assembly script
# xxx git version macro goes here
STARTTIME="$(date +%s)"
set -euo pipefail

# Usage information. Gets displayed if the arguments are not entered correctly.
usage()
{
cat << EOF
usage example: ./assemble-odm.sh -r medium -i roundhouse-skalanes/images -d 2 -e dkvart17@earlham.edu -f 3 -v -c &
-r    | --resolution        (required)            The quality of the mesh. Available options: lowest, low, medium, high, ultra
-i    | --images-directory  (required)            The relative path to the directory where the images live
-d    | --dimensions        (required)            Produces either 2D or 3D meshes. Available options: 2, 3
-e    | --email             (required)            Email address where the logs will be sent to
-f    | --filter            (default=1)           Run odm on a subset of images. E.g. -f 4 option will choose every 4-th image.
-v    | --verbose                                 More debugging output
-c    | --cleanup                                 If this job has already run, this option will delete the previous assembly
-h    | --help                                    Brings up usage
EOF
}

# Before exiting the script, successfully or not,
# delete the temporary directory.
cleanup() {
	if [[ ! -z ${TMP_IMAGES_DIRECTORY+x} && -d $TMP_IMAGES_DIRECTORY ]]; then
		echo "Removing $TMP_IMAGES_DIRECTORY"
		rm -rf $TMP_IMAGES_DIRECTORY
	fi
	cat $logfile | mailx -s "Your job $RUNID completed on $HOSTNAME" $EMAIL_ADDRESS
}
trap cleanup EXIT

# Default parameters
VERBOSE=false
CLEANUP=false
FILTER=1

# ARGUMENT PARSING AND VALIDATION
##############################################

# Parse the command line arguments.
while [[ $# -gt 0 ]]
do
arg="$1"

case $arg in
    -r|--resolution)
    	RESOLUTION="$2"
    	shift
    	shift
    ;;
    -i|--images-directory)
    	IMAGES_DIRECTORY="$2"
    	shift
    	shift
    ;;
    -d|--dimensions)
    	DIMENSIONS="$2"
    	shift
    	shift
    ;;
    -e|--email)
		EMAIL_ADDRESS="$2"
		shift
		shift
	;;
	-f|--filter)
		FILTER="$2"
		shift
		shift
	;;
	-v|--verbose)
		VERBOSE=true
		shift
	;;
	-c|--cleanup)
		CLEANUP=true
		shift
	;;
    -h|--help)
    	usage
    	exit 0
    ;;
    *)
    shift # Ignore the rest
    ;;
esac
done

# Checking if all required arguments were passed 
if [[ -z ${RESOLUTION+x} || -z ${IMAGES_DIRECTORY+x} || -z ${DIMENSIONS+x} || -z ${EMAIL_ADDRESS+x} ]]; then
	echo "You didn't set the parameters correctly!"
	usage
	exit 1
fi

# Checking if image directory exists
if [[ ! -d $IMAGES_DIRECTORY ]]; then
	echo "Directory $IMAGES_DIRECTORY does not exist!"
	usage
	exit 1
fi
##############################################


# PREPARING WORKING ENVIRONMENT TO RUN ODM
##############################################

# mesh quality
if [[ ( "$RESOLUTION" == "lowest" ) ]]; then
	MNF=3000
	MS=37500
	MOD=8
	QUALITY="lowest"
elif [[ ( "$RESOLUTION" == "low" ) ]]; then
	MNF=6000
	MS=75000
	MOD=9
	QUALITY="low"
elif [[ ( "$RESOLUTION" == "medium" ) ]]; then
	MNF=12000
	MS=150000
	MOD=10
	QUALITY="medium"
elif [[ ( "$RESOLUTION" == "high" ) ]]; then
	MNF=24000
	MS=300000
	MOD=11
	QUALITY="high"
elif [[ ( "$RESOLUTION" == "ultra" ) ]]; then
	MNF=36000
	MS=400000
	MOD=12
	QUALITY="ultra"	

else
	echo "unknown RESOLUTION" $RESOLUTION
	usage
	exit 1
fi

# dimensions 
if [[ ( "$DIMENSIONS" == 2 ) ]]; then 
	DIMS="--fast-orthophoto --orthophoto-png"

elif [[ ( "$DIMENSIONS" == 3 ) ]]; then 
	DIMS="--dsm --pc-ept"

else 
	echo "unknown DIMENSIONS" $DIMENSIONS
	usage
	exit 1
fi

RUNID="$DIMENSIONS"D-$RESOLUTION-f$FILTER

IMAGES_DIRECTORY="`cd "${IMAGES_DIRECTORY}";pwd`" #normalize the path
BASE_DIRECTORY=$(dirname $IMAGES_DIRECTORY)
OUTPUT_DIRECTORY=$BASE_DIRECTORY/$RUNID
logfile=$OUTPUT_DIRECTORY/log
TMP_IMAGES_DIRECTORY=$BASE_DIRECTORY/tmp

if [[ -d $OUTPUT_DIRECTORY ]]; then
	if [ "$CLEANUP" = true ]; then
		echo "Cleaning up... Deleting $OUTPUT_DIRECTORY"
		rm -rf $OUTPUT_DIRECTORY
	else
        echo "This combination of dimension, resolution, and filtering has already run, cancelling"
        echo "If you want to overwrite its output, re-run the command with the -c option."
		exit 1
	fi
	echo "================================================================================================"	
fi

mkdir $OUTPUT_DIRECTORY
mkdir $TMP_IMAGES_DIRECTORY

i=0
# filter out the images
for im in $IMAGES_DIRECTORY/*
do
	if [[ $(($i%$FILTER)) -eq 0 ]]; then
		cp $im $TMP_IMAGES_DIRECTORY
	fi
	i=$(($i+1))
done
NUM_IMAGES=$(ls $TMP_IMAGES_DIRECTORY | wc -l) 
#wc doesn't return an integer. Can evaluate like this: $((NUM_IMAGES))
##############################################

# print basic info to user
cat << EOF
Initializing ODM with the following parameters..
================================================================================================
RESOLUTION: $RESOLUTION
DIMENSIONS: $DIMENSIONS
NUM_IMAGES: $((NUM_IMAGES))
RUNID: $RUNID
IMAGES_DIRECTORY: $IMAGES_DIRECTORY
OUTPUT_DIRECTORY: $OUTPUT_DIRECTORY
EMAIL_ADDRESS: $EMAIL_ADDRESS
================================================================================================
EOF

# RUNNING ODM 
##############################################
ODM_COMMAND="docker run --detach \
-v \"$TMP_IMAGES_DIRECTORY:/code/images\" \
-v \"$OUTPUT_DIRECTORY/entwine_pointcloud:/code/entwine_pointcloud\" \
-v \"$OUTPUT_DIRECTORY/odm-orthophoto:/code/odm_orthophoto\" \
-v \"$OUTPUT_DIRECTORY/odm-texturing:/code/odm_texturing\" \
-v \"$OUTPUT_DIRECTORY/odm-dem:/code/odm_dem\" \
-v \"$OUTPUT_DIRECTORY/odm-meshing:/code/odm_meshing\" \
-v \"$OUTPUT_DIRECTORY/odm-georeferencing:/code/odm_georeferencing\" \
-v \"$OUTPUT_DIRECTORY/odm-report:/code/odm_report\" \
opendronemap/odm $DIMS \
--min-num-features $MNF --mesh-size $MS --mesh-octree-depth $MOD \
--pc-quality $QUALITY --feature-quality $QUALITY --time"

if [ "$VERBOSE" = true ]; then
	echo "Running the following command ==>"
	echo "$ODM_COMMAND"
	echo "================================================================================================"
	echo "Running ODM on these images ==>"
	ls -l $TMP_IMAGES_DIRECTORY
fi

CID=$(eval "$ODM_COMMAND")

echo
echo "Container ID: $CID"

docker wait $CID
docker logs $CID &> $logfile
docker rm $CID
##############################################

echo "================================================================================================"

DURATION=$[ $(date +%s) - ${STARTTIME} ]
echo "Duration (s): ${DURATION}" >> $logfile

echo "SUCCESS!"

exit 0
