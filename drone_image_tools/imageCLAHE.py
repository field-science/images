# A script to improve the contrast of an image using Contrast Limited Adaptive Histogram Equalization (CLAHE)
# Uses the OpenCV library which can be installed using pip install opencv-python

import cv2, argparse
import convertColorSpace

# SPECIFY COMMAND LINE ARGUMENTS
parser = argparse.ArgumentParser(description="image histogram equalization")
parser.add_argument('-i', '--input', help='Enter the input image', required=True, dest="input")
parser.add_argument('-o', '--output', help='Enter the name and location of the output image', required=True, dest="output")
parser.add_argument('-s', '--cspace', help='Enter the color space for the conversion to be applied in (YUV, HSV, LAB, or YCrCb)', required=True, dest="cspace")
parser.add_argument('-c', '--clipLimit', help='Enter the clip limit for the CLAHE', required=False, default=2.0, dest="climit")
parser.add_argument('-t', '--tileGridSize', help='Enter the tile grid size for the CLAHE', required=False, default=(8,8), dest="tgsize")
args = parser.parse_args()

# Load image
image = cv2.imread(args.input)

# Convert image to specified color space and split channels
image = convertColorSpace.convertFromBGR(image, args.cspace)
ch0, ch1, ch2 = cv2.split(image)

# Perform CLAHE on color channel that affects contrast
# Channel is index 2 for HSV, 0 for all others
clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8, 8))

if args.cspace=='HSV':
	eqChannel = clahe.apply(ch2)
	image = cv2.merge((ch0, ch1, eqChannel))
else:
	eqChannel = clahe.apply(ch0)
	image = cv2.merge((eqChannel, ch1, ch2))

# Convert image back to BGR color space
image = convertColorSpace.convertToBGR(image, args.cspace)

# Write image to specified output
cv2.imwrite(args.output,image)
