# README for ndvi_gui3.py
## Daniel Stoffregen djstoff15@earlham.edu
  
This script was written for python 3.x  and requires PIL, tkinter, and matplotlib.

> python3 -mpip install Pillow

> python3 -mpip install matplotlib

This script can be called from the command line with

> python3 ndvi_gui3.py

A window will then pop up:

* Select the image that you want to change
* Select the algorithm you want to apply
* Type in the name you want the new file to have, must include extension.
    * Such as “my_new_image.jpg”. Must be a file type that PIL supports. This
    * includes .jpg and .tif.
* Click convert. It can take a little bit.
* Four sliders and a save button will appear alongside two images. The sliders change different features of the image, and the save button records saves those changes.

Three outputs will be created, the converted image, and an image called "enhanced_my_new_image.jpg" and "final_enhanced_my_new_image.jpg".
* enhanced*.jpg is a 250x250 image used to quickly apply changes,
* final*.jpg is a full sized image with the changes applied by the sliders.

If you want to expand the code and add sliders to play with the colors of the images with an algorithm applied I think you can do it by creating an ndarray using matplotlib, and changing the values of different color bands in the array. 
