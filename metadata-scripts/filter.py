import sys
import os
import argparse
import shutil

'''
filters images based on the input number. Eg. if num = 3, it will create an output directory
with every 3rd image from the input directory
'''
def filter_images(image_dir, output_dir, num, mode):
    count = 0
    if os.path.isdir(output_dir):
        shutil.rmtree(output_dir)
    os.mkdir(output_dir)
    if mode == "keep":
        for image in sorted(os.listdir(image_dir)):
            if count % num == 0:
                src = image_dir+"/"+image
                dst = output_dir+"/"+image
                shutil.copyfile(src, dst)
            count += 1
    elif mode == "remove":
        for image in sorted(os.listdir(image_dir)):
            if count % num != 0:
                src = image_dir+"/"+image
                dst = output_dir+"/"+image
                shutil.copyfile(src, dst)
            count += 1
if __name__=="__main__":
    parser = argparse.ArgumentParser(description='Filters images')
    parser.add_argument('-i', '--inputfiles', nargs='+', type=str, help='Path to images')
    parser.add_argument('-o', '--outputfilename', type=str, help='Dir name to store output')
    parser.add_argument('-n', '--num', type=int, help='n-th')
    parser.add_argument('-m', '--mode', type=str, help='keep or remove')
    args = parser.parse_args()
    if args.inputfiles is None or args.outputfilename is None or args.num is None or args.mode is None:
        print("Error, provide all three arguments")
        exit(-1)
    filter_images(args.inputfiles[0], args.outputfilename, args.num, args.mode)
