#! /bin/sh

# Desc: Script to convert images to Thumbnails, to allow web scripts to load them easily
# Author: Ahsan Ali Khoja - aakhoja15@earlham.edu
# Date: Oct 2018

if [ $# -ne 1 ]; then
        echo "usage: ./createThumbs.sh <directory>"
        exit 1
fi

dir=$(echo $1 | sed 's/\/$//')

for file in $(find $dir -maxdepth 1 -type f -regextype posix-extended -iregex '.*\.(jpg|png|dng)' | grep -v 'Thumb-*');
do
	if [ ! -f "$dir/Thumb-$(basename $file).jpg" ]
	then
		convert -define jpeg:size=500x200 $file -auto-orient -resize 300x160 -unsharp 0x.5 -bordercolor white -border 5 -bordercolor '#ddd' -border 1 "$dir/Thumb-$(basename $file).jpg"
	fi
done
