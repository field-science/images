import numpy as np
from scipy.spatial import Delaunay
import argparse
import random
import math
import simplekml

class Triangle:
    def __init__(self, point1, point2, point3):
        self.p1 = point1
        self.p2 = point2
        self.p3 = point3

    def area(self) -> float:
        return 0.5 * abs((self.p1[0] * (self.p2[1] - self.p3[1])) + (self.p2[0] * (self.p3[1] - self.p1[1])) + (self.p3[0] * (self.p1[1] - self.p2[1])))

    def random_point(self) -> [float, float]:
        t = random.random()
        s = random.random()
        r_t = math.sqrt(t)
        a = 1.0 - r_t
        b = (1.0 - s) * r_t
        c = s * r_t
        return [(a * self.p1[0]) + (b * self.p2[0]) + (c * self.p3[0]), (a * self.p1[1]) + (b * self.p2[1]) + (c * self.p3[1])]

    def random_points(self, num) -> [[float, float]]:
        points = []
        for i in range(num):
            points.append(self.random_point())
        return points


parser = argparse.ArgumentParser(
    prog='random_lat_long',
    description="""generates random points uniformly on a convex polygon defined by the input points, 
    requesting fewer points than triangles in the polygons triangulation produces erroneous output""")

parser.add_argument("file")
parser.add_argument("out_file")
parser.add_argument("points")


args = parser.parse_args()

points = []

file = open(args.file)
lines = file.readlines()
for line in lines:
    s = line.split()
    points.append([float(s[1]), float(s[0])])
file.close()

points = np.array(points)
tri = Delaunay(points)

tris = []
for point in points[tri.simplices]:
    triangle = Triangle(point[0], point[1], point[2])
    tris.append(triangle)

out_points = []
area_total = 0
for t in tris:
    area_total += t.area()
for t in tris:
    frac = t.area()/area_total
    num_points = int(round(float(args.points) * frac))
    out_points = out_points + t.random_points(num_points)

kml = simplekml.Kml()
for p in out_points:
    kml.newpoint(coords= [(p[0], p[1])])
kml.save(args.out_file)
