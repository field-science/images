#by Caden Green, 14 Sep 2024
input_file_path = './solo-gcps.kml'
output_file_path = './solo-gcps-converted.txt'

coordinates_list = []
with open(input_file_path,'r') as infile:
    for line in infile:
        if line[0:13] == '<coordinates>':
            current_coordinates = line[13:].split('<',1)[0] #because I don't know how long the numbers will be, I split at the < and take the firstpart of the split
            current_coordinates = tuple(current_coordinates.split(','))
            coordinates_list.append(current_coordinates)
with open(output_file_path,'w') as outfile:
    outfile.write('EPSG:4326') #the google doc said that this was the correct number for the RTK, but it may need to be changed.
    current_gcp_number = 1
    for i in coordinates_list:
        outfile.write('\n')
        outfile.write(i[0]+' '+i[1]+' '+i[2]+' 0 0 gcp'+str(current_gcp_number))
        current_gcp_number += 1
