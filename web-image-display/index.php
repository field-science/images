<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<style>
			img:hover {
    				box-shadow: 0 0 2px 1px rgba(0, 140, 186, 0.5);
			}

			a.button{
				-webkit-appearance: button;
				-moz-appearance: button;
				appearance: button;

				text-decoration: none;
				color: initial;
				padding-top: 1;
				padding-bottom: 2;
				padding-right: 7;
				padding-left:7;
			}
			div#images{
				overflow: scroll;
			}
		</style>
	</head>
	<body>
		<?php include "/cluster/fieldscience/imageThumbnails.php"; ?>
	</body>
</html>
