# Functions to convert the color space of an image being processed using the OpenCV library
# Uses the OpenCV library which can be installed using pip install opencv-python
# Note: OpenCV works by default in BGR code rather than RGB for color images

import cv2

# Dictionary of color conversion codes for each color space currently supported
# entries are in form colorSpace: (conversion code to space, conversion code from space)
colorConversionCodes = {
	'YUV': (cv2.COLOR_BGR2YUV, cv2.COLOR_YUV2BGR),
	'HSV': (cv2.COLOR_BGR2HSV, cv2.COLOR_HSV2BGR),
	'LAB': (cv2.COLOR_BGR2LAB, cv2.COLOR_LAB2BGR),
	'YCrCb': (cv2.COLOR_BGR2YCrCb, cv2.COLOR_YCrCb2BGR)
}

def convertFromBGR(image, colorSpace):
	return cv2.cvtColor(image, colorConversionCodes[colorSpace][0])

def convertToBGR(image, colorSpace):
	return cv2.cvtColor(image, colorConversionCodes[colorSpace][1])
