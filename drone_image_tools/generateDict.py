import os, exifread

# Create a dictionary of key value pairs in the form [image_name]:[altitude_value]
# Altitude is actually elevation, as it measures the height from sea level

def gen(inputDir):
    # container vars
    jpegAltitudeDict = {}
    jpegList = []

    # Creates a list of image names
    for each_jpeg in os.listdir(inputDir): # iterate through each image in the input (-i) directory
        if each_jpeg[-3:] == "jpg" or each_jpeg[-3:] == "JPG" or each_jpeg[-4:] == "jpeg": # check for any kind of jpg ext
            jpegList.append(os.path.join(inputDir,each_jpeg))

    # Create a dictionary of pairs
    for imageName in jpegList: # iterate over the images collected and add altitude infomration
        f = open(imageName, 'rb')
        tags = exifread.process_file(f)
        for tag in tags.keys(): # find altitude tag
            if tag == ('GPS GPSAltitude'):
                jpegAltitudeDict[imageName] = eval("%s" % tags[tag])
                #might need to multiply by -1 if below sea level to make everything look right

    minVal = jpegAltitudeDict[min(jpegAltitudeDict)] #find minimum altitude

    for item in jpegAltitudeDict:
        jpegAltitudeDict[item] = jpegAltitudeDict[item] + (minVal * -1) # adjust altitude to be reletive

    return jpegAltitudeDict
