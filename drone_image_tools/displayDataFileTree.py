# Porter L : 6/14/19

# Meant for helping to organize assemblies of drone footage
# by showing the directory structure and checking for 2d and 3d assemblies
# by file format in their most likely locations.

# This script should be run in the parent directory with the following structure, or something equvalent.
# The script assumes that 


# STRUCTURE:

#/ PARENT
#| / LOCATION 1
#| | / SITE 1
#| | | / NIRI
#| | | | / 30m
#| | | | | > DATA
#| | | | | > ODM_ASSEMBLY_2D
#| | | | 
#| | | | / 50m
#| | | | | > DATA
#| | | | | > ODM_ASSEMBLY_2D
#| | | |
#| | | 
#| | | / VLI
#| | | | / 30m
#| | | | | > DATA
#| | | | | > ODM_ASSEMBLY_2D
#| | | | 
#| | | | / 50m
#| | | | | > DATA
#| | | | | > ODM_ASSEMBLY_2D
#| | | |
#| | |
#| |
#|

import os, argparse

# SPECIFY COMMAND LINE ARGUMENTS
parser = argparse.ArgumentParser(description="jpeg altitude histogram")
parser.add_argument('-i', '--input', help='Enter the input jpeg directory', required=True, dest="inDir")
args = parser.parse_args()

# COMMAND LINE ARGUMENT VARIABLES
inputDir = args.inDir

def main():
    if not os.path.isdir(inputDir):
        raise ValueError("Input directory does not exist, or is not a folder!")
    list_files(inputDir)
    

def list_files(startpath):
    for root, dirs, files in os.walk(startpath):
        level = root.replace(startpath, '').count(os.sep)
        indent = ' ' * 4 * (level)
        print('{}{}/'.format(indent, os.path.basename(root)))
        subindent = ' ' * 4 * (level + 1)

        filecount = 0
        is_3d = False
        is_2d = False
        for f in files:
            if f.endswith('.ply'):
                is_3d = True
            if f.endswith('.png') or f.endswith('.jepg'):
                is_2d = True

            filecount = filecount + 1
        if filecount != 0:
            print('{}{}'.format(subindent, str(filecount) + " files"))
            if 'ortho' in root:
                print('{}{}'.format(subindent, "2D Assmebly: " + str(is_2d)))
            if 'geo' in root:
                print('{}{}'.format(subindent, "3D Assmebly: " + str(is_3d)))



main()
