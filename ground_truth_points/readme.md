# Ground Truth Point Selection

This python program randomly distributes points inside of a region for determining sites to ground truth drone imagery. It takes 3 arguments, the path to an input file with points defining the region that points will be scattered within, the path to the output kml of distributed points, and the number of points to distribute. 

The input file should contain whitespace separated decimal latitude and longitude pairs, latitude first, longitude second, one pair per line. input_example.dat is an example of the required formatting. The program forms a convex hull through Delaunay Triangulation, concave regions will need to be separated into smaller convex pieces. In addition cases where the resulting triangulation has more triangles than distributed points, or contains degenerate triangles are not supported, if the program behaves erratically try increasing the number of points to distribute, or remove bounding points from the input.

Example usage:

python3 random_ground_truth.py input_example.dat output_example.kml 10

This script requires numpy, scipy, and the simplekml libraries
