for file in *.DNG
do
        dcraw -c $file | convert - "${file%.DNG}".jpg
        exiftool -tagsfromfile $file -all:all "${file%.DNG}".jpg
done