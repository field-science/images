import matplotlib.pyplot as plt
import numpy as np
import argparse
import generateDict

# SAMPLE COMMAND

# $ python3 jpegHistogram.py -i "<INPUT DIR>" -s 1 -m 10 -b 10

# SPECIFY COMMAND LINE ARGUMENTS
parser = argparse.ArgumentParser(description="JPG altitude histogram")
parser.add_argument('-i', '--input', help='Enter the input JPG directory', required=True, dest="inDir")
parser.add_argument('-b', '--bins', help='Enter number of bins to use', required=True, dest="bins")


args = parser.parse_args()

# COMMAND LINE ARGUMENT VARIABLES
inputDir = args.inDir
numBins = int(args.bins)

x = [] # values to plot
jpegAltitudeDict = generateDict.gen(inputDir) # get image dictionary

for image in jpegAltitudeDict:
    x.append(jpegAltitudeDict[image])

plt.xlabel('Altitude relative to flight (meters)')
plt.ylabel('Number of images captured')

increment = ( (max(x) +1) / numBins)
plt.xticks(np.arange(int(min(x)), int(max(x)) + 1, 3))

n, bins, patches = plt.hist(x, numBins, facecolor='blue', alpha=0.5)
#plt.show(block=True)
plt.savefig('histogram.png')
