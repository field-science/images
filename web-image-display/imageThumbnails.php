<?php
/*
Description: Php script to display image thumbnails. PhP version 5.3.3.
Author: Ahsan Ali Khoja
Date: July 2018
*/

include "/cluster/fieldscience/gpsCoord.php";
?>

<script src="http://cluster.earlham.edu/fieldscience/Leaflet_lib/src/leaflet.js"></script>
<link rel="stylesheet" href="http://cluster.earlham.edu/fieldscience/Leaflet_lib/src/leaflet.css" />
<script src="http://cluster.earlham.edu/fieldscience/Leaflet_lib/src/leaflet_awesome_number_markers.js"></script>
<link rel="stylesheet" href="http://cluster.earlham.edu/fieldscience/Leaflet_lib/src/leaflet_awesome_number_markers.css" />
<script>
	var markers = L.layerGroup();

	function createMarker(lat, lon, imgNum){
//		console.log(imgNum, lat, lon); 
		L.marker([lat, lon],{
			icon: new L.AwesomeNumberMarkers({
				number: imgNum,
				markerColor: "darkblue"
			})
		}).addTo(markers);
	}
</script>
<div style="height: 100%;">
	<div id="images" style="height: 40%">

<?php
$dir = './';
$imageArray = array();
foreach(glob($dir.'*') as $fname){
	$fileType = mime_content_type($fname);
	if ($fileType == "directory"){
?>
		<a href="<?php echo basename($fname); ?>" class="button"><?php echo basename($fname); ?></a>
<?php
	}
	/* elseif (reset(explode("/",$fileType)) == 'image'){
		$imageArray[] = basename($fname);
	} */
}
echo "<br>";
// foreach($imageArray as &$imageName){
$counter = 1;
foreach(glob($dir.'Thumb-*.jpg') as $imageName){
	$imgExif = exif_read_data($imageName);
	$imgLon = getGps($imgExif["GPSLongitude"], $imgExif['GPSLongitudeRef']);
	$imgLat = getGps($imgExif["GPSLatitude"], $imgExif['GPSLatitudeRef']);
	print("geocode: ");
	print_r($imgLat);
	print(", ");
	print_r($imgLon);
	print(" ");
	echo "<script type=\"text/javascript\"> createMarker($imgLat, $imgLon, $counter);</script>";
?>
	<?php echo $counter; ?><a target="_blank" href="<?php echo substr(ltrim(basename($imageName), 'Thumb-'), 0, -4); ?>">
		<img src="<?php echo basename($imageName); ?>" alt="None" style="width:250px">
	</a>
<?php
	$counter++;
}
?>
	</div>
	<div id="mapdiv" style="height:  60%;"></div>
</div>
<script>
        var map = L.map("mapdiv").setView([<?php echo $imgLat;?>, <?php echo $imgLon; ?>], 16);
        L.tileLayer('https://tile.thunderforest.com/landscape/{z}/{x}/{y}.png?apikey={api_key}', {
                attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.thunderforest.com/">Thunderforest</a>',
                maxZoom: 20,
                id: 'thunderforest.streets',
                api_key: '264d2d59c3b24731ade829c875f614df'
        }).addTo(map);
	markers.addTo(map);
</script>
