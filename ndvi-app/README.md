#ndvi-app
ndvi-app is a web-based visualization tool for displaying and analysing NIR images. 

Front end is implemented in React and backend is a python flask app.


#Initial setup instructions:

cd to ndvi-app

Frontend:
Install the latest version of node.js from nodejs.org
Node comes with npm, so run => npm install => to install all the react packages
After that, run => npm build run => which will produce a bundle

Backend:
The flask app needs to be run with python3

Run the command below to install python dependencies =>
python3 -m pip install -r ndvi-backend/requirments.txt


#Running the app:

python3 ndvi-backend/api.py

