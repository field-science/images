#!/usr/bin/env bash
set -euo pipefail

#CAVEATS
#1.assemble-odm.sh should be in the same directory as run-batch-odm.sh
#2.you need relative path to queue file
#3.In the queue file, make sure last line is separated by enter
#4.

queue=$(pwd)/$1

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )" #copied from stack overflow

if [[ ! -f $queue ]]; then
	echo "Directory $queue does not exist! ABORT..."
	exit 1
fi


while IFS= read -r next_args
do
	echo "FROM BATCH SCRIPT: Running with the following args: $next_args"
	nohup $SCRIPT_DIR/assemble-odm.sh $next_args &
	ID=$!
	wait $ID
done < "$queue"

echo "SUCCESS"

exit 0