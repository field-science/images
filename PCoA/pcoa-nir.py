'''
An incomplete PCA running script for images using the library sklearn for PCA algorithm, 
the problems are marked in the annotation below
Materials might be helpful:
https://scikit-learn.org/stable/modules/generated/sklearn.decomposition.PCA.html
https://plot.ly/ipython-notebooks/principal-component-analysis/
http://dilloncamp.com/projects/pca.html
'''

'''Issue 1, argparse argument need to be added after the script is finished and according to 
how we are going to use it'''
from sklearn.decomposition import PCA
from sklearn.preprocessing import normalize
from PIL import Image
import numpy as np

inputImage = Image.open('DJI_0719') #The image used as example

#split the RGB image into three color tunnels, they are all "L"
r,g,b = inputImage.split()

#transform the image info to matrix using numpy
X_r = np.array(r)
X_g = np.array(g)
X_b = np.array(b)

#Run PCA on the image data with the ammount of vector wanted, from 0 to 1, here 90% will be kept as an example
pca = PCA(.90)

lower_dimension_data_r = pca.fit_transform(X_r)
lower_dimension_data_g = pca.fit_transform(X_g)
lower_dimension_data_b = pca.fit_transform(X_b)

'''Issue 2, when reducing the redundant vector, the number remained for each tunnel is different, 
see from running the command below
print(lower_dimension_data_r.shape)
print(lower_dimension_data_g.shape)
print(lower_dimension_data_b.shape)

When transforming the reduced matrix back to the original size, it seems that now only the size of the
smallest tunnel could be used, which is B tunnel in this case with value 264

So all three RGB tunnels can only use B tunnel for transforming back, which means they are the same now, 
I assume that means I lost the data from other two tunnels but not sure, needs to be solved'''
approximation_r = pca.inverse_transform(lower_dimension_data_b)
approximation_g = pca.inverse_transform(lower_dimension_data_b)
approximation_b = pca.inverse_transform(lower_dimension_data_b)

#generate tunnel image using the matrix after PCA
tunnel_r = Image.fromarray(approximation_r)
tunnel_g = Image.fromarray(approximation_g)
tunnel_b = Image.fromarray(approximation_b)

#merge the three tunnels back
'''Issue 3, the three tunnels can't be merged together, even they have the same mode "L" and size'''
outputImage = Image.merge('RGB',(tunnel_r,tunnel_g,tunnel_b))
outputImage.show()