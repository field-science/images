# built/tested with python 3.3 on pollock 

# Note that this performs a nonsensical update to the data, it's only an example.

import psycopg2

connectionString = "host='hopper.cluster.earlham.edu' dbname='field_science' user='charliep' password='test'"
connection = psycopg2.connect(connectionString)
	
cursor1 = connection.cursor()
sqlStatement1 = "\
	select serial_number, exif_gpsaltitude, exif_flightyawdegree \
	from image_metadata \
	where exif_directory = 'Iceland-2016/images-structured/Skalanes/Skalanes_Survey_2016'"
cursor1.execute(sqlStatement1)

cursor2 = connection.cursor()
sqlStatement2 = "\
	update image_metadata set ne_cornerlatitude = %s where serial_number = %s"

for record in cursor1:
#	print(record)
	serial = record[0]
	NECornerLatitude = record[1] + record[2]
#	print(NECornerLatitude)
	cursor2.execute(sqlStatement2, (NECornerLatitude, serial))
	connection.commit()

cursor1.close()
cursor2.close()
connection.close()	
exit()